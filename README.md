# jekyll-pandoc repo

This is a repo that can serve as a basis for GitLab [Jekyll Pandoc](https://github.com/mfenner/jekyll-pandoc) sites.

For the original GitLab jekyll repo, see [https://gitlab.com/pages/jekyll](https://gitlab.com/pages/jekyll)